# README #
This is a series of 6 heavily commented (and hopefully self explanatory) C++ projects which you can go through to improve your understanding of the interface between high level C++ and the way it's implemented at the machine level.

Each project has its own readme.txt and the comments in it should also explain it enough for you to make sense of it :)

Related: here is a zip containing a set of blogs about this stuff which take it much further (you may need internet explorer to open the files!)
https://drive.google.com/file/d/15XVZzMR3yvRjr2vAeUgmIkCAEJVsl4Y3/view?usp=sharing



# LICENSE for this project #
Copyright 2020 Alex Darby Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.