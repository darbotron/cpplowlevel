////////////////////////////////////////////////////////////////////////////////////////////////////////////
// If you have not done so already whilst working with this project file, please open and read ReadMe.txt
////////////////////////////////////////////////////////////////////////////////////////////////////////////
#include "HelperMacros.h"


////////////////////////////////////////////////////////////////////////////////////////////////////////////
// program entry point
int main( int argc, char* argv[] )
{
	PRINT_FUNCTION_NAME;
	int x = 1; 
	int y = 2;
	int z = 0;

#pragma region instructions
	// If you're running this yourself you'll need to:
	// 1) run the program 
	//
	// 2) when the breakpoint is hit:
	//		a) switch to disassembly view (right click this FORCE_BREAKPOINT and choose "Go To Disassembly" 
	//		from the context menu)
	//
	//		b) select the "Watch 1" pane in the left hand side of the split panes at the bottom - make sure 
	//		you have a watch set up for:
	//			* eax
	//			* x
	//			* y
	//			* z
	//
	//		c) open the 'Viewing Options' tab at the top of the disassembly window an make sure that only
	//		the 'show source code' and 'show address' checkboxes are checked
	//
	// 3) continue reading the comments until they tell you to single step the debugger - this is denoted 
	// by [STEP_DEBUGGER] in a comment
#pragma endregion instructions for running outside of the session

	////////////////////////////////////////////////////////////////////////////////////////////////////////
	// 1) How CPUs work, and not being scared of the disassembly window.
	//
	FORCE_BREAKPOINT;	
	// Before we even start the session it's important that we all understand a little about how CPUs work.
	//
	// Rather than bore you with diagrams, we're going to do it the simple way - by demonstration.
	//
	// The purpose of this session is not to teach you how to write programs in assembler, but part of its 
	// purpose IS to show you not to fear it! 
	//
	// The disassembly window tells you what is ACTUALLY happening when code you write is executed - this is
	// often invaluable information, and being familiar with disassembly will give you a much better chance
	// of tracking down any bugs that you might get only in release builds of your code.
	//
	// The next line of code after this comment should correspond to approximately the following assembler:
	// 
	// mov	eax,dword ptr [ebp-XXh] 
	// add	eax,dword ptr [ebp-YYh]
	// mov	dword ptr [ebp-ZZh],eax
	//
	// So what does it mean?
	//
	// * "mov" and "add" are mnemonics - each represents a CPU instruction, one per line with its arguments
	// * "eax" and "ebp" are two of the CPU registers in the x86 CPU architecture 
	// * "ebp-XXh", "ebp-YYh", and "ebp-ZZh" are the memory addresses storing the values of x, y, and z
	//	 you can see this if you 
	// * "dword ptr[...]" means "the 32 bit value stored in the address between the square brackets"  
	// 
	// Registers are "working area" for CPUs like tiny little bits of memory that are built into the CPU 
	// itself, and which the CPU can access "instantaneously". Rather than having addresses like memory, 
	// registers are named - because there are usually only a (realtively) small number of them. 
	//
	// Some registers are involved in controlling program execution and others are "general purpose". In 
	// order for the CPU to operate on a value it must typically be copied from its memory address into a 
	// (general purpose) register, manipulated and then written back to the memory address.
	//
	// Now, with one eye on the disassembly, and the other on the "Auto" window...
	// [STEP_DEBUGGER]	note that the "eax" watch has changed to the value of x (i.e. the 32 bit value 
	//					stored at ebp-XXh)
	// [STEP_DEBUGGER]	note that the "eax" watch has changed to its old value plus the value of y
	// [STEP_DEBUGGER]	note that the "z" watch has changed to the value of eax
	z = x + y;

	// this forces a breakpoint, using a magical windows function call __debugbreak()
	FORCE_BREAKPOINT;

	// So, what's the point of looking at this tiny snippet of assembler?
	//
	// Well the first thing to note is that the C++ concept of a variable (or any other language's concept 
	// of a variable for that matter) doesn't exist at the assembler level. In assembler the values of x, y, 
	// and z are stored in specific memory addresses, and the CPU gets at them by explicit use of their 
	// respective memory addresses. The high level language concept of a variable, whilst easier to think 
	// about, is 
	// actually already an abstracted concept identifying a value in a memory address. 
	// 
	// Secondly, note that in order to do anything "interesting" (e.g. add a value to another) the CPU needs 
	// to have at least part of the data it is operating on in a register.
	//
	// Finally, this extremely simple example illustrates one of the most important facts about programming: 
	// that high level languages exist only to make life easy for humans, they're not any kind of accurate 
	// reflection of how CPUs actually work - in fact, if you think about it, even assembler is a human 
	// convenience compared to the actual binary opcodes that the assembler mnemonics (e.g. mov, add etc.) 
	// represent. 
	//
	// My advice is don't think about it too much, and definitely don't worry about the electrons.

	WAIT_TIL_ENTER_PRESSED;

	return 0;
}
