////////////////////////////////////////////////////////////////////////////////////////////////////////////
// If you have not done so already whilst working with this project file, please open and read ReadMe.txt
////////////////////////////////////////////////////////////////////////////////////////////////////////////
#include "HelperMacros.h"


////////////////////////////////////////////////////////////////////////////////////////////////////////////
// program entry point
int main( int argc, char* argv[] )
{
	PRINT_FUNCTION_NAME;
	int x = 1; 
	int y = 2;
	int z = 0;

	////////////////////////////////////////////////////////////////////////////////////////////////////////
	// 5) C++ object model. The broad anatomy of C++ objects and an overview of how they work.
	//						
	// construction / destruction and inheritance
	// - memory layout in single inheritance, 
	// - memory layout in multiple inheritance. Demo behaviour of 'this' 
	// pointer when calling methods of base types.
	// - how polymorphism is implemented
	// - virtual fns not available during con / destruction

	//
	// Note if you want to see the layout of all the types in this project output to the build output window 
	// you can put this in the command line flags for the compiler in the project options /d1reportAllClassLayout
	// if you just want a specific class use /d1reportAllClassXXX where XXX = the exacat typename
	// 

	// As with most things in C++ the internal working of objects is in fact really very simple.
	// 
	// The memory layout of a class is determined primarily by the textual order that its data members are
	// declared in the class declaration. The number of and ordering of functions has no effect on this.
	// 
	class CMemoryLayoutDemo
	{
		int		iFirst;
		float	fSecond;
		char	chThird;
		int		iFourth;
	};


	// We know that on win32 int and float are both 4 bytes, and that char is 1 byte so intuitively one 
	// would assume that CMemoryLayoutDemo would be 13 bytes in size. In all likelihood, however, this is 
	// not the case.
	//
	// Some platforms require certain types to be aligned to specific memory boundaries, almost all run
	// signifcantly faster if values are aligned to specific boundaries and windows is one of them.
	//
	// The size of this struct is, in fact 16 bytes, because - at the very least - 32 bit x86 CPUs like to 
	// have things 4 byte aligned (32bit which is a "word" - i.e. "natural unit of data processing" for the 
	// 32bit x86 CPUs).
	PRINT_MARKER( "int, float, char, int = ? bytes" );
	PRINT_VALUE( sizeof( CMemoryLayoutDemo ));
	FORCE_BREAKPOINT;

	// Memory layout in inheritance is also simple, and decided by the textual order of the code.
	class CBaseOne
	{
		int	iOne;
	public:
		int GetBaseOne( void ) { return iOne; }
	};

	class CBaseTwo
	{
		float fTwo;
	public:
		float GetBaseTwo( void ) { return fTwo; }
	};

	class CDerived
	: public CBaseOne
	, public CBaseTwo
	{
		int		iThree;
		float	fFour;
	public:
		int		GetThree( void )	{ return iThree; }
		float	GetFour( void )		{ return fFour; }
	};

	// the memory layout of CDerived will be like this
	//
	//	+==============  CDerived  =================+
	//	|	CBaseOne::iOne				(4 bytes)	|
	//	+-------------------------------------------+
	//	|	CBaseTwo::fTwo				(4 bytes)	|
	//	+-------------------------------------------+
	//	|	CDerived::iThree			(4 bytes)	|
	//	|	CDerived::fFour				(4 bytes)	|
	//	+===========================================+
	//
	PRINT_BLANKLINE;
	PRINT_MARKER( "Memory Layout" );
	PRINT_VALUE( sizeof( CBaseOne ));
	PRINT_VALUE( sizeof( CBaseTwo ));
	PRINT_VALUE( sizeof( CDerived ));
	FORCE_BREAKPOINT;


	// another interesting thing about memory layout is the implications for the value of pointers
	// during implicit type conversions. Consider the following code:
	CDerived	cMyDerived;

	CBaseOne*	pBaseOne	= &cMyDerived;
	CBaseTwo*	pBaseTwo	= &cMyDerived;
	CDerived*	pMyDerived	= &cMyDerived;

	// These pointers have all been set from the address of the same object, but they don't
	// point to the same address...
	PRINT_BLANKLINE;
	PRINT_MARKER( "Implicit conversion of CDerived pointer to base types." );
	PRINT_VALUE( pBaseOne );
	PRINT_VALUE( pBaseTwo ); // points to a different address
	PRINT_VALUE( pMyDerived );
	FORCE_BREAKPOINT;

	// Witchcraft? No. Again it's really just common sense.
	//
	// When the compiler generates the code for each class' functions it must do so based purely on the 
	// memory layout of the class it operates on - so, for example, CBaseTwo::GetBaseTwo() only knows 
	// about CBaseTwo, and the offset that CBase::fTwo is at relative to a CBaseTwo pointer.
	//
	// This means that when we access a CDerived via a CBaseTwo* it has to adjust the address to point 
	// to the CBaseTwo part of it. This isn't necessary for a CBaseOne* because CBaseOne is the first
	// part of the memory layout of a CDerived, and so the pointer is in the same place.


	// The only thing other than the data members of a class that affect its size is whether or not it has 
	// virtual functions.
	//
	// This is because the mechanism by which virtual functions are implemented in C++ is to have a lookup 
	// table for each class' virtual functions. This lookup table is generally refered to as the vtable.
	// 
	// In order to allow each class instance access to the vtable, a hidden pointer to the vtable is 
	// inserted in the class' memory layout.
	//
	// This class is identical to CMemoryLayoutDemo, except that it now has a vtable, and it's size has gone 
	// up 16 to 20 bytes.
	class CMemoryLayoutVirtualDemo
	{
		int		iFirst;
		float	fSecond;
		char	chThird;
		int		iFourth;
		virtual ~CMemoryLayoutVirtualDemo(){}	
	};
	PRINT_BLANKLINE;
	PRINT_MARKER( "int, float, char, int, + vtable = ? bytes" );
	PRINT_VALUE( sizeof( CMemoryLayoutVirtualDemo ));
	FORCE_BREAKPOINT;

	// Typically, the vtable is inserted at the front of the class' data, but since there is nothing in the 
	// C++ standard that enforces this you should never rely on it. In fact the codewarrior compiler used on 
	// the Wii puts the vtable at the end of the class' data.
	//
	// How do virtual function calls involving vtables work? We'll get to that :)


	// Construction order redux!
	// As we discussed in the Cpp Primer session, call order of the constructors of base classes is 
	// dependent on the order the derived class inherits them - and this order is identical to the order of 
	// the objects in memory layout.
	//
	// This means that all base constructors are guaranteed to be called before the ones of classes that 
	// derive from them, and that the memory layout of an object is initialised from the "front" to the 
	// "back" at the same time.
	//
	// A further implication of this is for the use of virtual functions in constructors.
	//
	class CBaseVirtual
	{
	public:
		virtual ~CBaseVirtual( void )			{}
		virtual void VirtualFunction( void )	{PRINT_FUNCTION_NAME;}
		CBaseVirtual( void )					
		{
			PRINT_FUNCTION_NAME;
			VirtualFunction();
		}
	};

	class CDerivedVirtual
	: public CBaseVirtual
	{
	public:
		virtual void VirtualFunction( void )	{PRINT_FUNCTION_NAME;}
	};

	// When we create an instance of CDerivedVirtual, the natural assumption would be that the call to 
	// VirtualFunction in CBaseVirtual::CBaseVirtual would resolve to CDerivedVirtual::VirtualFunction.
	//
	// This isn't the case however. Since CDerivedVirtual's constructor hasn't been called yet, it is 
	// in an undefined state and so the C++ standard (and common sense) dictate that during constructors
	// any function belonging to the class that would ordinarily be virtual will be a regular, non virtual,
	// function call when called from the constructor. 
	PRINT_BLANKLINE;
	PRINT_MARKER( "call VirtualFunction in constructor of CBaseVirtual..." );
	CDerivedVirtual cMyDerivedVirtual;


	// How do virtual function calls involving vtables work?
	//
	// Regular function calls are resolved at compile time (or link time), which means that the assembly 
	// generated specifies exactly where to jump to in order to execute each function. With a virtual 
	// function call, the compiler cannot resolve the exact function that will be called at compile time. 
	// Instead of a specific address, virtual function calls resolve to a specific offset into the vtable. 
	//
	// So, for example, in the case of CBaseVirtual it's vtable would contain two entries, one for the 
	// destructor and one for VirtualFunction. Let's assume that these would be at index 0 and 1 
	// respectively in the vtable.
	//
	//	+======= CBaseVirtual ===========+
	//	|	vtable* ---------------------+--->	+===========================================+
	//	+================================+		| CBaseVirtual::~CBaseVirtual				|
	//											| CBaseVirtual::VirtualFunction				|
	//											+===========================================+
	//
	// In the case of an instance of CDerivedVirtual, its vtable would also contain 2 entries: 
	// 
	//	+====== CDerivedVirtual =========+
	//	|	vtable* ---------------------+--->	+===========================================+
	//	+================================+		| CDerivedVirtual::~CDerivedVirtual			|
	//											| CDerivedVirtual::VirtualFunction			|
	//											+===========================================+
	// 
	// So, the compiler would resolve any call to VirtualFunction to the function stored in vtable[ 1 ].

	WAIT_TIL_ENTER_PRESSED;

	return 0;
}
