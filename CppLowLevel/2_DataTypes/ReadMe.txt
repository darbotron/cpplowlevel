////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Session Overview 
// 
// For this session, we're going to look at implementational detail of C++ - i.e. look (in overview) how C++ 
// operates at the machine (i.e. assembly language) level. 
//
// Firstly, let's deal with why C++ is used for games when it's such an old fashioned language and lots of 
// bad stuff is said about it?			
//
// The reasons are pretty simple and come down to 2 key considerations:
//
// 1) Convenience - the console hardware has historically changed every 5 years or so, and C (and then C++) 
// are the simplest "high level" languages to get working on a new computer architecture.
//
// 2) Fitness for purpose - C was explicitly designed for writing operating systems (C++ was really just an 
// OO extension of C to start with, though it has become so much more than that) and they allow you to do 
// anything that you could do in assembler.  
//
// If used correctly, C/C++ allow you to write code that can be as fast as hand coded assembler, but with 
// all the convenience, abstraction, and human readability of a high level language. C/C++ also allow you to 
// (relatively) easily combine asm code modules with C++ code, and even use inline assembler in C/C++ code.
//
// We typically use C++ rather than C because it offers the best compromise of OO language features with 
// C's freedom: 
// - pro: C++ has complete freedom for allocating and using system resources
// - pro: no imposed memory usage models, and you can write your own
// - con: plenty of rope to hang yourself with
// - con: no "built in" language support for complex data types
// - pro: no "built in" language support for complex data types
//
//
// So, this is what we're going to cover in this session:
//
// 1) A little look into how CPUs work, and how not to be scared of the disassembly window.
//
// 2) Data Types. What intrinsic and non-intrinsic types are, why it matters, and the sizes of types.
//
// 3) Pointers. What they are, why they're necessary, why they're not scary, and why arrays start from 0.
//
// 4) The Stack. What it is, what it does, what's stored there, and the implications of this.
//
// 5) C++ object model. The broad anatomy of C++ objects and an overview of how they work.
//						
// 6) Memory. There are 5 areas of memory used in C++. We discuss them here.
//