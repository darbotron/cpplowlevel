////////////////////////////////////////////////////////////////////////////////////////////////////////////
// If you have not done so already whilst working with this project file, please open and read ReadMe.txt
////////////////////////////////////////////////////////////////////////////////////////////////////////////
#include "HelperMacros.h"


////////////////////////////////////////////////////////////////////////////////////////////////////////////
// program entry point
int main( int argc, char* argv[] )
{
	PRINT_FUNCTION_NAME;
	int x = 1; 
	int y = 2;
	int z = 0;

	FORCE_BREAKPOINT;

	////////////////////////////////////////////////////////////////////////////////////////////////////////
	// 2) Data Types. What intrinsic, non-intrinsic, and built-in types are, and why it matters.
	//
	// The types of values that can be stored and manipulated in the registers of a CPU are called its 
	// intrinsic types. Specifically, to qualify as an intrinsic type, the CPU must have specific 
	// instructions for dealing with that type of data.
	//
	// Typically, the limiting factors that decide the intrinsic types for a CPU are the size of the 
	// registers the CPU has, and whether it is economically and computationally viable to dedicate some of 
	// the silicon the chip is made from to the operations required by that type of value.
	//
	// Handling any non-intrinsic data type will require the compiler (or assembler programmer) to write 
	// code that defines the interactions of the non-intrinsic type in terms of the CPU's intrinsic types.
	//
	// For example; the first console I worked with that had intrinsic floating point support was the Sega 
	// Dreamcast - before that all non-integer maths was either fixed point, or accomplished by implementing
	// it by hand in assembler in terms of integers. 
	// 
	// The compilers for most consoles also have special support for extra non-standard hardware intrinsics 
	// that are built into the console hardware - e.g. the PS3 and X360 compilers both have a special 
	// 4-32bit-floats-represented-in-a-128-bit-value vector type, as did the PS2. Using these types 
	// appropriately can give maths intensive code a big performance boost - but certain operations on 
	// some hardware run even slower than not using the intrinsics, so you have to be careful.
	//
	// C++ has "built in types" which represent a subset of the possible intrinsic types for CPUs that C++
	// code might be compiled for:
	//
	// integral:		char, unsigned char, short, unsigned short, int, unsigned int, long, unsigned long
	// floating point:	float, double, long double
	// boolean:			bool
	// void:			void can't be used as a variable; it is used to represent the type of a 
	//					"typeless" pointer
	// pointer: 		a pointer is a type that stores the memory address of a variable
	// arrays:			you may also have an array of any one of the above types 		
	//
	// The only guarantee that we have in the C++ standard (which comes from C) about size of variables is 
	// that int has the natural size suggested by the system architecture (one "word") and the four integer 
	// types char, short, int and long must each one be at least as large as the one preceding it, with char 
	// being always one byte in size. The same applies to the floating point types float, double and long 
	// double, where each one must provide at least as much precision as the preceding one.
	//
	// These days you are more or less guaranteed that all built in types will be represented directly by 
	// an intrinsic type on the target CPU that you compile your C++ code for.
	//
	PRINT_VALUE( sizeof( char ) );
	PRINT_VALUE( sizeof( unsigned char ) );
	PRINT_VALUE( sizeof( int ) );
	PRINT_VALUE( sizeof( unsigned int ) );
	PRINT_VALUE( sizeof( short ) );
	PRINT_VALUE( sizeof( unsigned short ) );
	PRINT_VALUE( sizeof( long ) );
	PRINT_VALUE( sizeof( unsigned long ) );

	PRINT_VALUE( sizeof( float ) );
	PRINT_VALUE( sizeof( double ) );
	PRINT_VALUE( sizeof( long double ) );

	FORCE_BREAKPOINT;

	// The final important thing to note about built in types is that their sizes vary from platform to 
	// platform. This might sound odd, but it's for the best. It makes sense that the physical data type 
	// that used to represent int should always be the fastest available signed integer format on a given 
	// hardware architecture, and this varies between CPUs (at least it has done over time...).
	//
	// However, we often want to know what size a particular variable is so we can control the sizes of data
	// files that we write / send across the internet etc. And know that the data will be readable in the 
	// expected format across several different sets of hardware. A very common way to get around this is to 
	// use #define to create a set of preprocessor definitions that can be specified in a header for each 
	// platform and used to read and write data of specific sizes.
	//
	// For example in your engine code, in Platform_PC.h you might have ( N.B. the keyword typedef allows an 
	// alternative name to be specified for a type):
	// 
	typedef char			i8;
	typedef unsigned char	u8;
	typedef short			i16;
	typedef unsigned short	u16;
	typedef int				i32;
	typedef unsigned int	u32;
	typedef float			f32;
	
	// this would allow the following code... 
	u8 au8Array[ 32 ] = { 0 };
	// ...which is clearly much less verbose than it's non-typedef'd equivalent
	unsigned char auchrray[ 32 ] = { 0 };

	// These typedefs would be duplicated in another platform specific header for each platform, and allow
	// each the platform code base to declare variables of specific sizes.




	// One last thing to cover in this is what is POD - Plain Old Data.
	//
	// The simplest answer to this is "a POD type is any type that can safely be copied using the standard C 
	// library function memcpy()."
	//
	// memcpy() does a naive bytewise copy of a specific number of bytes from one pointer into another. That
	// part is easy, it's the "safely" copied part that isn't that simple...
	//
	// With respect to the C++ definition of POD, the following characteristics instantly make a type 
	// non-POD:
	//
	//	* derived from any class.
	//	* user defined constructor.
	//	* user defined destructor.
	//	* user defined copy constructor.
	//	* user defined assignment operator.
	//	* private or protected (non static) member data.
	//	* (non static) member data that is non POD.
	//	* has a vtable - i.e. any virtual functions.
	//
	// Additionally, just to complicate matters, a type can technically be composed of POD but still be 
	// semantically non-POD if it stores pointers to dynamic memory that it is responsible for freeing.
	//
	// It is a general rule of good practice in C++ that types declared with the struct keyword should be 
	// kept POD. Another good rule of thumb is that structs should never contain classes.

	WAIT_TIL_ENTER_PRESSED;

	return 0;
}
