#ifndef _CPPPRIMER_HELPERMACROS_H_
#define _CPPPRIMER_HELPERMACROS_H_

////////////////////////////////////////////////////////////////////////////////////////////////////////////
// What these helper macros are, and how they work will be explained in the CppAdvanced and / or 
// CppCompilerPrimer sessions. For now please consider them magic for now, or look "C++ preprocessor macro" 
// up on the interweb if you're desperate to find out :)
////////////////////////////////////////////////////////////////////////////////////////////////////////////
#include <iostream>
#include <list>


////////////////////////////////////////////////////////////////////////////////////////////////////////////
// a namespace to seal all this code away
////////////////////////////////////////////////////////////////////////////////////////////////////////////
namespace HelperMacros
{
	////////////////////////////////////////////////////////////////////////////////////////////////////////
	// class that auto-indents printing proportionally to the number of instances of this class currently in 
	// scope.
	// N.B. you should use this only via the macros in this file...
	// N.N.B. this code is TOTALLY not threadsafe.
	////////////////////////////////////////////////////////////////////////////////////////////////////////
	class CAutoIndenter
	{
	private:
		enum
		{
			EIndentArraySize	= 32, 

			EIndentCharStep		= 2,
		};

		// the name of the function	- passed in at construction
		const char* m_pstrFunctionName;

		////////////////////////////////////////////////////////////////////////////////////////////////////
		// static class variable for the current number of characters of indent
		// N.B. this is a sneaky nonstandard way to do static class member data variable that prevents the 
		// need for tedious initialisation nonsense in a .cpp file
		// N.N.B. this wouldn't be advisable for multithreaded code, or use with libraries 
		////////////////////////////////////////////////////////////////////////////////////////////////////
		static int& GetIndentCharCount( void )
		{
			static int s_iIndentCharCount = 0;
			return s_iIndentCharCount;
		}

		////////////////////////////////////////////////////////////////////////////////////////////////////
		// static class variable that holds the actual indent string
		////////////////////////////////////////////////////////////////////////////////////////////////////
		static char* InternalGetIndentString( void )
		{
			static char s_achIndentString[ EIndentArraySize ] = { '\0' };
			return &( s_achIndentString[ 0 ] );
		}

		////////////////////////////////////////////////////////////////////////////////////////////////////
		// this function increases the size of the indent string by EIndentCharStep.
		// N.B. it will not increase the array size if adding EIndentCharStep would exceed the array size, 
		// this keeps the indent an integral multiple of EIndentCharStep.
		////////////////////////////////////////////////////////////////////////////////////////////////////
		void IncreaseIndent( void )
		{
			// N.B. because we init the whole array to '\0', we only ever need to add new spaces
			// as long as we dont overwrite the last char in the array.
			int iMaxUseableIndex = EIndentArraySize - 1;

			if( GetIndentCharCount() < iMaxUseableIndex )
			{
				int iStartIndex	= GetIndentCharCount();
				int iLimitIndex	= iStartIndex + EIndentCharStep;

				// never exceed the last useable index
				// n.b. this test always maintains the max multiple of EIndentCharStep that fits
				if( iLimitIndex < iMaxUseableIndex )
				{
					for( int iLoop = iStartIndex; iLoop < iLimitIndex; ++iLoop )
					{
						InternalGetIndentString()[ iLoop ] = ' ';
					}

					// set new index count
					GetIndentCharCount() = iLimitIndex;
				}
			}
			else
			{
				// print a warning that code attempted to exceed array size
				std::cout<<__FILE__<<__LINE__<< " CAutoIndenter: maximum indent size hit, please increase EIndentArraySize";
			}
		}

		////////////////////////////////////////////////////////////////////////////////////////////////////
		// decreases the indent by EIndentCharStep
		////////////////////////////////////////////////////////////////////////////////////////////////////
		void DecreaseIndent( void )
		{
			if( GetIndentCharCount() > 0 )
			{
				int iLimitIndex	= GetIndentCharCount();
				int iStartIndex	= iLimitIndex - EIndentCharStep;
																 
				// we only increment in exact steps of EIndentCharStep, so shouldn't need a < 0 check
				for( int iLoop = iStartIndex; iLoop < iLimitIndex; ++iLoop )
				{
					InternalGetIndentString()[ iLoop ] = '\0';
				}

				// set new index count
				GetIndentCharCount() = iStartIndex;
			}
		}

	public:
		////////////////////////////////////////////////////////////////////////////////////////////////////
		CAutoIndenter( const char* pstrFunctionName )
		: m_pstrFunctionName ( pstrFunctionName )
		{
			std::cout<< GetIndentString() << m_pstrFunctionName << "\n" << GetIndentString() << "{\n";
			IncreaseIndent();
		}


		////////////////////////////////////////////////////////////////////////////////////////////////////
		~CAutoIndenter( void )
		{
			DecreaseIndent();			
			//std::cout<< GetIndentString() << "} // end " << m_pstrFunctionName << "\n\n";
			std::cout<< GetIndentString() << "}\n\n";

		}


		////////////////////////////////////////////////////////////////////////////////////////////////////
		const char* GetIndentString( void )
		{
			return InternalGetIndentString();
		}
	};


	////////////////////////////////////////////////////////////////////////////////////////////////////////
	// class that tracks the stack, and any local variables added to it	
	class CStackFrame
	{
	private:
		//////////////////////////////////////////////////////////////////////////
		// static interface to track the frames
			typedef std::list< const CStackFrame* >				lstFrameTracker;
			typedef std::list< const CStackFrame* >::iterator	itrFrameTracker;

			//////////////////////////////////////////////////////////////////////////
			// gets a reference to the list of trackers
			static lstFrameTracker& TrackingList()
			{
				// created the first time this is called.
				static lstFrameTracker s_lstFrameTracker;
				return s_lstFrameTracker;
			}

			//////////////////////////////////////////////////////////////////////////
			// add/remove this instance to/from the tracking list
			void TrackSelf()
			{
				TrackingList().push_back( this );
			}

			void UntrackSelf()
			{
				TrackingList().remove( this );
			}		
		
		//////////////////////////////////////////////////////////////////////////
		// regular member data etc.
			const char*	m_pstrFunctionName;	// name
			void*		m_pESP;				// stack pointer
			void*		m_pEBP;				// stack base pointer

			//////////////////////////////////////////////////////////////////////////
			// list of locals
			struct CLocal
			{
				const char* _pstrName;
				void*		_pAddress;

				CLocal( const char* pstrName, void* pAddress )
				: _pstrName( pstrName )
				, _pAddress( pAddress )
				{}
			};

			typedef std::list< CLocal >				lstLocals;
			typedef std::list< CLocal >::iterator	itrLocals;
			lstLocals m_lstLocals;

			lstLocals& GetLocals( void )
			{
				return m_lstLocals;
			}

	public:

		// construct
		CStackFrame( const char* pstrName, void* pESP, void* pEBP )
		: m_pstrFunctionName	( pstrName )
		, m_pESP				( pESP )
		, m_pEBP				( pEBP )
		{
			TrackSelf();
		}

		// destruct
		~CStackFrame()
		{
			UntrackSelf();
		}

		// add local variable
		void AddLocal( const char* pstrName, void* pAddress )
		{
			m_lstLocals.push_back( CLocal( pstrName, pAddress ) );
		}

		// print out the stack
		static void PrintStack( const char* pstrIndent )
		{
			// stack header
			std::cout	<< "\n"
						<< pstrIndent << "################################################################\n"
						<< pstrIndent << "Current Call Stack\n\n"
						<< pstrIndent << "Current Fn: " << TrackingList().back()->m_pstrFunctionName << "\n"
						<< pstrIndent << "Root Fn:    " << TrackingList().front()->m_pstrFunctionName << "\n\n"
						<< pstrIndent << "+==================    'BOTTOM' OF STACK    ===================+\n";

			// base values of esp / ebp
			void* pBaseESP = TrackingList().front()->m_pESP;
			void* pBaseEBP = TrackingList().front()->m_pEBP;

			int iFrameCount = 0;
			for(	itrFrameTracker itrFrames = TrackingList().begin();
					itrFrames != TrackingList().end();
					++itrFrames, ++iFrameCount )
			{
				const CStackFrame* pFrame = *(itrFrames);

				// print the stack frame delimiter on all but the 1st loop
				if( pFrame->m_pESP != pBaseESP )
				{
					std::cout << pstrIndent	<< "+--------------------------------------------------------------+\n";
				}

				// print the name and pointers
				std::cout	<< pstrIndent	<< "| [ Frame " << iFrameCount << " ] >> " << pFrame->m_pstrFunctionName << "\n"
							<< pstrIndent	<< "|\n"
							<< pstrIndent	<< "| [ Stack Pointer: " << ((unsigned char*)pFrame->m_pESP - (unsigned char*)pBaseEBP )
											<< " ][ Base Pointer: " << ((unsigned char*)pFrame->m_pEBP - (unsigned char*)pBaseEBP ) << " ]\n";
				
				// if there are locals for this stack frame print those too.
				if( pFrame->m_lstLocals.size() )
				{
					std::cout << pstrIndent << "| Locals:\n";
					lstLocals& rLocals = const_cast< CStackFrame* >( pFrame )->GetLocals();
					for(	itrLocals itrLocal = rLocals.begin();
							itrLocal != rLocals.end();
							++itrLocal )
					{
						int iStackOffset = ((unsigned char*)itrLocal->_pAddress ) - ((unsigned char*) pBaseEBP );
						int iFrameOffset = ((unsigned char*)itrLocal->_pAddress) - ((unsigned char*) pFrame->m_pEBP );
						std::cout	<< pstrIndent	<< "|   offsets [ stack: " << iStackOffset 
													<< " ][ frame: " << iFrameOffset << " ]\t" << itrLocal->_pstrName << "\n";
					}
				}

				std::cout << pstrIndent << "|\n";
			}

			std::cout	<< pstrIndent << "+==================     'TOP' OF STACK      ===================+\n"
						<< pstrIndent << "\n"
						<< pstrIndent << "N.B. The stack starts at the TOP of memory and grows DOWN in\n"
						<< pstrIndent << "address space, so the 'top' of the stack is below the 'bottom'\n"
						<< pstrIndent << "in terms of the actual memory addresses it corresponds to.\n"
						<< pstrIndent << "################################################################\n";
		}

	};

}//namespace HelperMacros


////////////////////////////////////////////////////////////////////////////////////////////////////////////
// captures the stack pointer
////////////////////////////////////////////////////////////////////////////////////////////////////////////
#define CAPTURE_STACKPOINTER( intoVariable )			{ __asm mov intoVariable, esp }


////////////////////////////////////////////////////////////////////////////////////////////////////////////
// captures the stack base pointer
////////////////////////////////////////////////////////////////////////////////////////////////////////////
#define CAPTURE_BASEPOINTER( intoVariable )				{ __asm mov intoVariable, ebp }


////////////////////////////////////////////////////////////////////////////////////////////////////////////
// creates a local variable of type HelperMacros::CAutoIndenter with the function signature as its name
// also creates data to track the stack and locals involved with it too
// N.B. most PRINT_... macros rely on this macro being used before them in each function
////////////////////////////////////////////////////////////////////////////////////////////////////////////
#define PRINT_FUNCTION_NAME				HelperMacros::CAutoIndenter hlprcLocalIndenter( __FUNCSIG__ );	\
										void* hlpr_pESP = 0;											\
										void* hlpr_pEBP = 0;											\
										CAPTURE_STACKPOINTER( hlpr_pESP );								\
										CAPTURE_BASEPOINTER( hlpr_pEBP );								\
										HelperMacros::CStackFrame hlprLocalStackFrame( __FUNCSIG__, hlpr_pESP, hlpr_pEBP )


////////////////////////////////////////////////////////////////////////////////////////////////////////////
// adds a local to the stack tracker 
////////////////////////////////////////////////////////////////////////////////////////////////////////////
#define STACK_TRACKER_ADD_LOCAL( variable )		hlprLocalStackFrame.AddLocal( #variable, &variable )


////////////////////////////////////////////////////////////////////////////////////////////////////////////
// prints the current stack
////////////////////////////////////////////////////////////////////////////////////////////////////////////
#define PRINT_CURRENT_STACK				HelperMacros::CStackFrame::PrintStack( hlprcLocalIndenter.GetIndentString() )


////////////////////////////////////////////////////////////////////////////////////////////////////////////
// prints an identifier and its value correctly indented
////////////////////////////////////////////////////////////////////////////////////////////////////////////
#define PRINT_VALUE( identifier )		(std::cout<<hlprcLocalIndenter.GetIndentString()<<#identifier<<": "<<identifier<<"\n")


////////////////////////////////////////////////////////////////////////////////////////////////////////////
// prints the specified (c-style string) as marker text correctly indented
////////////////////////////////////////////////////////////////////////////////////////////////////////////
#define PRINT_MARKER( string )			(std::cout<<hlprcLocalIndenter.GetIndentString()<<"########## "<<string<<" ##########\n")


////////////////////////////////////////////////////////////////////////////////////////////////////////////
// prints a blank line
////////////////////////////////////////////////////////////////////////////////////////////////////////////
#define PRINT_BLANKLINE					(std::cout<<"\n")


////////////////////////////////////////////////////////////////////////////////////////////////////////////
// prints out the current values of esp and ebp - i.e. stack pointer and base pointer registers
////////////////////////////////////////////////////////////////////////////////////////////////////////////
#define PRINT_STACK_INFO				(std::cout << hlprcLocalIndenter.GetIndentString() << "ESP[" << hlpr_pESP << "]  EBP[" << hlpr_pEBP << "]\n")


////////////////////////////////////////////////////////////////////////////////////////////////////////////
// adds a place for a breakpoint to be set without having to add arbitrary code
////////////////////////////////////////////////////////////////////////////////////////////////////////////
#define BREAKPOINTABLE_LOCATION			__asm{ nop }


////////////////////////////////////////////////////////////////////////////////////////////////////////////
// forces a breakpoint
////////////////////////////////////////////////////////////////////////////////////////////////////////////
#define FORCE_BREAKPOINT				(__debugbreak())

////////////////////////////////////////////////////////////////////////////////////////////////////////////
// waits for enter to be pressed before continuing execution
////////////////////////////////////////////////////////////////////////////////////////////////////////////
#define WAIT_TIL_ENTER_PRESSED			std::cout<<"\nPress Enter To Quit....";\
										std::cin.get()
#endif