////////////////////////////////////////////////////////////////////////////////////////////////////////////
// If you have not done so already whilst working with this project file, please open and read ReadMe.txt
////////////////////////////////////////////////////////////////////////////////////////////////////////////
#include "HelperMacros.h"


//////////////////////////////////////////////////////////////////////////
// prototypes for functions used to demo the call stack
void	DemoCallStack01( void );
void	DemoCallStack02( int iParameter );
int		DemoCallStack03( void );


////////////////////////////////////////////////////////////////////////////////////////////////////////////
// program entry point
int main( int argc, char* argv[] )
{
	PRINT_FUNCTION_NAME;
	int x = 1; 
	int y = 2;
	int z = 0;

	STACK_TRACKER_ADD_LOCAL( x );
	STACK_TRACKER_ADD_LOCAL( y );
	STACK_TRACKER_ADD_LOCAL( z );

	////////////////////////////////////////////////////////////////////////////////////////////////////////
	// 4) The stack. Stack frames, local variables, and why it's a very very bad idea to return a pointer to 
	// a local (i.e. stack) variables by address. 
	//
	// When a function is called, the program jumps to the memory address containing the code for the 
	// function, executes that code and then returns to where it was. This behaviour is fundamental to all 
	// modern programming languages, but how does it work? The answer is: the Stack.
	//
	// The Stack (or Call Stack) is a chunk of memory that, together with a few special registers on the CPU, 
	// maintains the complete execution state of the program (strictly speaking this is only true for single 
	// threaded programs); and, as such, it forms a core part of the mechanisms by which all computers 
	// operate (n.b. there might be stackless computers out there, but I've never used one).  
	//
	// The Stack, as its name suggests is a stack data structure. The execution state of the program within
	// any given function is stored in the Stack inside a structure called a Stack Frame. When a function is
	// called, its stack frame is put onto the top of the stack, and when it returns its stack frame is 
	// removed and the stack frame of the function that called it is reinstated at the top of the stack.
	//
	// At any given point in a program's execution it is possible to trace back down the stack and inspect 
	// the stack frames that are currently stored in it. This is how debuggers typically extract the 
	// information shown in the "call stack" window. 
	// 
	// The specifics of exactly what is stored, how it is stored, and when are beyond the scope of this 
	// session, and depend on all sorts of things - who wrote the compiler, the compiler settings specified 
	// by the user etc. there are several standard "calling conventions" and it's definitely worth looking 
	// it up on wikipedia if you're interested, as each has different advantages and disadvantages.
	//
	// In general, for games, we are primarily concerned with the speed of execution of the code generated 
	// by the compiler	which means that most game code (certainly on consoles) uses variations on the 
	// "fastcall" calling convention which emphasises the use of CPU registers, as opposed to the stack, for 
	// passing parameters to functions.
	//
	// Stack formats vary in data content, and therefore size, but all stack formats must have at least 2 
	// things in common as part of the mechanism by which the callstack operates: each frame must contain 
	// the local variables of the function and the return address for the program to jump to when the 
	// function has finished executing. 
	//
	// The stack frame on the top of the stack holds less information than the frames below it because 
	// a significant chunk of the information needed to restore a stack frame to the top of the stack is 
	// made up of instantaneous state information about CPU registers etc. which isn't known until 
	// immediately before another function is called. 
	//
	// In the case of small functions that are called very frequently, storing and restoring the execution 
	// state into the stack before each call and extracting it again afterwards becomes a significant 
	// overhead - which is why the keyword 'inline' was invented.
	// 
	// One extremely common "gotcha" in C/C++ programming is with regards to the use of pointers and/or 
	// references to local variables contained within the Stack. As a byproduct of the stack system the 
	// state of the memory contained in the stack frame of a function is guaranteed to be maintained until 
	// the function is removed from the stack. 
	//
	// This implies that it's ok to pass pointers or references to local variables of a function as 
	// parameters to any function called inside it, since its local variables will remain in scope until 
	// after the calling function returns and it's stack frame is taken off the stack.
	// 
	// However, once a function has returned, the memory containing its stack frame will subsequently be 
	// used to store the stack frames of other functions and so will be overwritten by unspecified data. 
	// Obviously this means it's a very, very bad idea to write any code that might leave a pointer or 
	// reference to a local variable stored somewhere after they might have gone out of scope.
	//
	// In fact, this is a lot easier to do by accident than you might think - and these sorts of issues can 
	// be very difficult to track down if they don't break straight away. The big problem with stack memory 
	// is that, depending on the program flow - specifically the number of stack frames in the tack and the 
	// sizes of their local data - it can be a very variable amount of time between the pointer / reference 
	// being stored and the data at the address it specifies being overwritten; which means that the data 
	// may remain "valid" for some time despite the fact that it's technically in an undefined state.
	//
	// There are lots of architectural solutions to this problem, as well as defensive programming 
	// techniques (e.g. check pointers passed to functions to see if they're in the stack, and if so check 
	// that they're within the currently valid area of the stack), but in general just remember to be wary 
	// of passing pointers or references to local variables to any code without knowing what that code does 
	// with them.

	// This function, and its child function calls demonstrate the call stack in action (exciting!)
	DemoCallStack01();

	WAIT_TIL_ENTER_PRESSED;

	return 0;
}
		   

//////////////////////////////////////////////////////////////////////////
// used to demonstrate how the call stack is laid out in memory
void DemoCallStack01( void )
{
	PRINT_FUNCTION_NAME;

	int iLocalA = 0;
	int iLocalB = 0;
	STACK_TRACKER_ADD_LOCAL( iLocalA );
	STACK_TRACKER_ADD_LOCAL( iLocalB );

	DemoCallStack02( iLocalA );
}


//////////////////////////////////////////////////////////////////////////
// used to demonstrate how parameters to functions are laid out in stack
void DemoCallStack02( int iParameter )
{
	PRINT_FUNCTION_NAME;
	STACK_TRACKER_ADD_LOCAL( iParameter );

	int iAnotherLocalA = 0;
	int iAnotherLocalB = 0;
	STACK_TRACKER_ADD_LOCAL( iAnotherLocalA );
	STACK_TRACKER_ADD_LOCAL( iAnotherLocalB );

	iAnotherLocalA = DemoCallStack03();
}


//////////////////////////////////////////////////////////////////////////
// used to demonstrate parameter passing in call stack
int DemoCallStack03( void )
{
	PRINT_FUNCTION_NAME;
	int iReturn = 0;
	STACK_TRACKER_ADD_LOCAL( iReturn );
	PRINT_CURRENT_STACK;
	FORCE_BREAKPOINT;
	return iReturn;
}