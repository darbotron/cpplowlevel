////////////////////////////////////////////////////////////////////////////////////////////////////////////
// If you have not done so already whilst working with this project file, please open and read ReadMe.txt
////////////////////////////////////////////////////////////////////////////////////////////////////////////
#include "HelperMacros.h"


////////////////////////////////////////////////////////////////////////////////////////////////////////////
// program entry point
int main( int argc, char* argv[] )
{
	PRINT_FUNCTION_NAME;
	int x = 1; 
	int y = 2;
	int z = 0;

	FORCE_BREAKPOINT;
	////////////////////////////////////////////////////////////////////////////////////////////////////////
	// 3) Pointers. Why they're necessary, why they're not scary, and why array indices start from 0
	// 
	// People who don't use C++ say bad things about pointers. Pointers are unsafe! Pointers are evil! 
	// Pointers do medical experiments on baby rabbits!
	//
	// Almost all these things are true, but the fact remains that pointers are a core part of the machine 
	// level instructions executed by CPUs. As a result, all programming languages - no matter how "safe" 
	// they are at the high level - must make use of them in their underlying implementation.
	//
	// There is nothing complicated about a pointer - it is simply a type whose value is a memory address. 
	// In order to do something useful with a pointer, it needs to be set to the memory address at which the 
	// value of a type is stored. If the pointer points to a type whose size is greater than 1 byte 
	// (which almost all types are), then its value will be the memory address containing the first byte of 
	// the type's value.
	//
	// I think it's worth quickly inserting a nugget of programming wisdom here - I'm bringing this up here, 
	// because finding this out was, for me, one of those eureka moments where loads of programming things 
	// I'd just taken at face value started to suddenly make sense. 
	//
	// In C and C++ the identifier of an array is (more or less) the same as a pointer to the first byte of 
	// the memory containing the array. This makes the built in implementation of the index operator very
	// simple. For the following code (where array is an array of int of size > 3):
	//
	//		array[ 3 ] = 4;
	//
	// the compiler will just add 3 * sizeof( int ) to the address that 'array' points at, and assign the 
	// value 4 to it treating it as an integer.
	//
	// This means you can also use the [] operator on pointers, which is actually a pretty common thing to 
	// do when you're dealing with low level code in situations where memory is at a premium (e.g. graphics 
	// data, network data etc.).
	// 
	// Looking at the following code in the disassembly window shows the array index operator in action...
	const int k_iArraySize = 8;
	int iArray[ k_iArraySize ];

	iArray[ 0 ] = 0;

	FORCE_BREAKPOINT;

	for( int iLoop = 0; iLoop < k_iArraySize; iLoop++ )
	{
		// ooh look! it's just multiplying 4 [which is sizeof( int )] by iLoop
		iArray[ iLoop ] = iLoop;
	}

	WAIT_TIL_ENTER_PRESSED;

	return 0;
}
