////////////////////////////////////////////////////////////////////////////////////////////////////////////
// If you have not done so already whilst working with this project file, please open and read ReadMe.txt
////////////////////////////////////////////////////////////////////////////////////////////////////////////
#include "HelperMacros.h"


/////////////////////////////////////////////////////////////////////////////
// class used to demonstrate construction of objects declared at global scope
class CGlobal
{
	int m_iData;
public:
	CGlobal( void )
	: m_iData( 0 )
	{
		PRINT_FUNCTION_NAME;
	}
};
CGlobal g_cGlobal;


////////////////////////////////////////////////////////////////////////////////////////////////////////////
// program entry point
int main( int argc, char* argv[] )
{
	PRINT_FUNCTION_NAME;
	int x = 1; 
	int y = 2;
	int z = 0;

	////////////////////////////////////////////////////////////////////////////////////////////////////////
	// 6) Memory. There are 5 areas of memory used in C++. We discuss them here.
	//
	// A) const data
	// =============
	// The data area is available throughout the lifetime of the program - the data stored in this area is 
	// typically part of the executable file.
	//
	// This area contains String literals and other compile time constants, as well as any initialised 
	// instances of built in types that are declared at global scope - it can only contain POD types whose 
	// initial data is known at the start of program execution.
	//
	// This area of memory can be further classified into two sub areas: 
	// * read only, which contains only constants
	// * read-write, which contains initialised types that are not constant
	//
	// Consider these declarations (assuming they're made at global scope):
	//
	// const int	iGlobalConst	= 1;	// i will be stored in the read only area.
	//
	// int			iGlobal			= 2;	// j will be stored in the read-write area
	//
	// // pString will be stored in the read-write area, and "string literal" in the const area
	// const char*	pString			= "string literal";

	// n.b. the string literals defined here will be stored in the const data section, the two const char* 
	// declared here that point to them will not.
	// n.n.b. since the 2nd string is a substring of the 1st that ends int he same place, the compiler would 
	// be well within its rights to just store the literal once and use it for both of them.
	const char* k_pstrStingLiteralOne	= "You had better not try to change this at runtime!";
	const char* k_pstrStingLiteralTwo	= "change this at runtime!";
	FORCE_BREAKPOINT;


	// B) Global & Static
	// ==================
	// This area of memory is used for non-constant types which exist at global scope, or are declared as 
	// static at local scope.
	//
	// The memory for the type instances stored in the Global & Static area is allocated at program startup, 
	// but initialised later.
	//
	// Global objects: 
	//	*	Initialised after program startup, but before main() executes. This can cause problems if 
	//		instances of a type that uses new / delete / malloc / free / calloc / realloc in its constructor 
	//		are declared at global scope.
	//
	//	*	In general construction order is "undefined", but globals in the same logical compilation block 
	//		will be constructed in textual order which gives a loophole for controlling this if you need to.
	//
	// Static (but not global)
	//	*	Initialised the first time the code declaring them is executed.
	//	*	Static local scope variables can be a good way to allow a function to have large local data use,
	//		without using loads of stack space. 
	//	*	If the type used by the function has a constructor that involves complex initialisation or 
	//		dynamic memory allocation then making it a local scope static will significantly speed up all 
	//		but the first call of that function. However, this would probably be more appropriately done by 
	//		making it a class member or class static and initialising it explicitly.
	class CStaticConstruction
	{
		int m_iData;
	public:
		CStaticConstruction( void )
		: m_iData( 0 )
		{
			PRINT_FUNCTION_NAME;
			PRINT_VALUE( this );
		}
	};
	FORCE_BREAKPOINT;
	static CStaticConstruction s_cStaticConstruction;


	// C) The Stack
	// ============
	// As discussed above, the Stack (or Call Stack) stores automatic (i.e. local) variables.
	//
	// The allocation of local variables is typically close to optimally fast, but when using types with 
	// constructors the calls to their constructors can sometimes slow things down.
	//
	// Types with constructors / destructors will be constructed when declared, and destructed when they go 
	// out of scope. 
	//
	// In general it is best to avoid using large types or types with complex construction / destruction as 
	// locals, as the stack is usually kept quite small on consoles / mobile devices - i.e. usually in the 
	// 10s or 100s of Kb rather than Mb.
	//
	// Memory overwrites (bad array indexing, dead pointers etc.) are particularly dangerous in the stack
	// because they will not only overwrite data in the stack but can also break the stack entirely, which 
	// will not only kill your program, but typically also leave you without a valid call stack.
 	FORCE_BREAKPOINT;


	// D & E) The Heap and Free Store 
	// ==============================
	// The Heap and the Free Store are defined in the C++ standard as two separate areas of memory, however
	// I'm covering them together because they're more or less identical with regards to their use cases and 
	// inherent gotchas.
	//
	// The Heap is the area of memory controlled by the standard C library dynamic memory functions: malloc, 
	// free, calloc, and realloc. Memory in this area is in one of two states: 
	//	* allocated (i.e. in use)
	//	* free (i.e. not in use)
	//
	// The Free Store is the area of memory controlled by the C++ operators new and delete. Memory in the 
	// Free Store can be in one of 4 states:
	//	* allocated - but not yet constructed
	//	* constructed 
	//	* destructed - destructed but not yet free
	//	* free
	//
	// FreeStore is typically implemented in terms of Heap, but should still be treated as separate. For 
	// example one compiler may happily let your code run if you use new to allocate some memory and then 
	// use free() to free it, but others will almost certainly crash.
	//
	// The characteristics of these two areas of memory are basically the same from an in game use-case 
	// point of view:
	//	* Dynamic memory is slow to allocate. The speed implications of this are significant enough that, 
	//	in general, allocation and freeing of memory should be considered a gameplay initialisation / 
	//	shutdown task. There should be no dynamic memory use during active gameplay.
	//
	//	* Since dynamically allocated memory is entirely under the control of the programmer(s) writing 
	//	the code it should always be treated as "inherently dangerous" and all dynamic memory use must be 
	//	handled with extreme care; especially in multi-threaded software. 
	//
	//	* Common problems with dynamic memory use include
	//		- leaking memory. This is occurs when memory is not deallocated after being freed, and will 
	//		eventually result in failed memory allocations. The best way to avoid this is to always write 
	//		the deallocation code immediately after the allocation code.
	//
	//		- dead pointers. Dead pointers occur when a pointer to a block of memory is still in use after 
	//		it has been deallocated. This will result in memory overwrites which may break the Heap / Free 
	//		Store and crash the program.
	//
	//		- double freeing. This is when something attempts to deallocate using a dead pointer. Typically
	//		this will crash the system unless C++ exceptions are turned on.
	//
	//		- fragmentation. Even if the first two are avoided, fragmentation can still result in out of 
	//		memory issues. Fragmentation typically occurs as a result of temporary dynamic allocations being 
	//		interspersed with long term dynamic allocations. This results in "holes" in the Heap/Free Store. 
	//
	//		Since all memory allocations must be contiguous (i.e. unbroken) ranges of addresses, and 
	//		allocated memory cannot be moved around, these holes can only be used to store allocations small 
	//		enough to fit inside them. Since the objects allocated in the holes are statistically unlikely 
	//		to be exactly the same size as the hole, this sort of memory use profile usually results in a 
	//		Heap / Free Store that has many tiny holes in it.
	//
	//		These holes may add up to a very significant amount of the theoretically available space managed 
	//		by the Heap / Free Store. For example in a very fragmented Heap there might be 5Mb free out of 
	//		64Mb, but nothing bigger than a few hundred Kb can be allocated in it.
	//
	//		Heap fragmentation issues can really only be prevented by architectural solutions and forward 
	//		planning. Typically the most important area to manage to prevent fragmentation is loading data 
	//		into memory from disc as this process often involves some sort of processing that requires 
	//		temporary allocations.
	//
	//	Helpfully, the repetitive flow of a game from Menus into Gameplay and back is a very effective way 
	//	to cause any small memory leak or fragmentation issue to turn into a game breaking problem so they
	//	are usually easy enough to find, but not necessarily easy to fix.
	//

	WAIT_TIL_ENTER_PRESSED;

	return 0;
}
